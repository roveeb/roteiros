Hoje iremos aprender a montar uma nuvem própria utilizando o nextcloud, docker e lets encrypt, na digitalocean.
O nextcloud é um sistema de nuvem gratuito e open source. O nextcloud nasceu de divergências no owncloud. Funciona de modo multi plataforma.
Para esse tutorial vamos precisar de um domínio e um servidor na hospedagem que você preferir.

## Primeiro comando:
```bash
adduser nomedousuario
```

## Segundo comando: - Adicionar o usuário ao grupo sudo
```bash
usermod -aG sudo nomedousuario
```

## Terceiro comando:
```bash
apt update
```
## Quarto comando:
```bash
apt install ufw
ufw allow OpenSSH
sudo ufw allow 80,443/tcp
ufw enable
ufw status
```

# Instalando Docker

## Instalar os pré requisitos do docker
```bash
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```
## Adicionar a GPG key
```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
## Adicionar repositorio
```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```
```bash
sudo apt update
```
## Verificar se o APT encontrou o pacote do docker
```bash
apt-cache policy docker-ce
```
## Instalar o docker
```bash
sudo apt install docker-ce
sudo systemctl status docker
```
## Baixando o docker-compose
```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
## Dando permissões
```bash
sudo chmod +x /usr/local/bin/docker-compose
```

## Clonar o repositório do Vitor Mattos, CEO Da Lyseon Tech
```bash
git clone https://github.com/LyseonTech/nextcloud-docker
```
### Criar uma rede
```bash
sudo docker network create reverse-proxy
```
### Iniciar a configuração do docker
Depois disso, precisamos alterar as variaveis `VIRTUAL_HOST`, `LETSENCRYPT_HOST` e `LETSENCRYPT_EMAIL` no documento docker-compose.yml.
Precisamos também setar uma senha para o banco no arquivo .env
### Iniciar a execução
Agora, podemos rodar o projeto com:
```bash
sudo docker-compose up -d
sudo docker-compose -f docker-compose.proxy.yml up -d
```
### Configuração web
Com o docker configurado e executando, agora vamos acessar o domínio definido e terminarmos a configuração. Não iremos alterar a pasta de destino, escolheremos o banco PostgreSQL e iremos colocar as informações:
- Nome do banco: nextcloud
- Senha do banco: senha definida no .env
- Usuário do banco: nextcloud
- Endereço do banco: db
 
Agora, só clicar em concluir configurações e aproveitar!

//see you later
