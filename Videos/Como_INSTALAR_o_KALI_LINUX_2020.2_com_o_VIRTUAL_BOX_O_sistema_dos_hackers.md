O Kali Linux é uma distribuição GNU/Linux baseada no Debian e mantido pela Offensive Security. É considerado o sucessor do backtrack e voltado para profissionais da segurança da informação.  
O Kali não foi feito para ser utilizado como distro pessoal, pois não conta com uma usabilidade boa e nem com segurança. A maioria dos comandos e ferramentas utilizadas são em modo root. O Kali também não é recomendado para iniciantes. Seu principal diferencial é possuir as ferramentas já pré instaladas, o que facilita e muito o dia a dia de um profissional da segurança.

## Para instalar o VirtualBox no windows

Basta fazermos download e executarmos.

## Para instalar o VirtualBox no Linux

- Iremos atualizar o apt

```bash
sudo apt update
```

- Agora basta instalar o vb

```bash
sudo apt install virtualbox
```

## Como instalar o Kali no VirtualBox

Basta fazermos download da ISO do Kali Linux e começarmos a instalação no VirtualBox!