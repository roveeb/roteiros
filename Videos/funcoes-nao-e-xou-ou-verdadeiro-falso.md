## Introdução
Hoje iremos dar continuidade no nosso curso de LibreOffice Calc, chegando na nossa aula de número 9! O nosso objetivo nesse vídeo será entender as outras 4 funções lógicas que são mostradas na documentação oficial.
--- Pausa para pedir like e inscreva-se ---
Bom, as funções de hoje são simples e servem como um complemento para o vídeo sobre a função SE.
## Desenvolvimento
### Função NÃO
A função NÃO, é uma função simples. Ela complementa, ou inverte, um teste lógico.
Sua sintaxe é simples, basta colocarmos um =NÃO(Teste) e teremos o retorno negativo. Se teste for positivo, então o retorno será negativo.
### Função VERDADEIRO e FALSO
A função VERDADEIRO sempre retorna um teste lógico como verdadeiro. Para utilizarmos não precisamos passar argumentos, ficando a fórmula da seguinte maneira =VERDADEIRO(Teste). Sempre teremos o retorno como verdadeiro.
Já a função FALSO, funciona de maneira inversa a função verdadeiro, possuindo a mesma sintaxe, =FALSO(Teste) e retornando sempre um teste lógico como falso.
#### --- Pedir inscrição --- 
### Função E
Essa função necessita de mais de um teste lógico. Caso todos os testes forem verdadeiros, retorna VERDADEIRO. Caso um dos testes for falso, retorna falso, mesmo que os outros sejam verdadeiros.
Sintaxe: =E(Teste A;B;C)
### Função OU
Essa função também necessita de mais de um teste lógico e há comparação entre os valores. Caso, no mínimo, um valor for verdadeiro, o retorno será verdadeiro.
Sua sintaxe é: =OU(Teste 1; Teste 2; Teste 3)
### Função XOU
A última função de hoje, a função XOU, retorna verdadeiro se um número impar de testes lógicos forem verdadeiros, ou seja, caso tenhamos 6 testes, se 3 deles forem verdadeiro, o retorno será verdadeiro.

## Finalização
Então, chegamos ao fim de mais um vídeo, hoje aprendemos mais algumas funções lógicas, porém essas são menos utilizadas do que a função SE.
Queria aproveitar para agradecer todo o carinho de vocês, pois essa série, por mais que não tenha muitas visualizações, está tendo retornos maravilhosos do ponto de vista da educação. Várias pessoas, quando conversam comigo, falam que estão utilizando dessa série para aprender sobre o LibreOffice do zero. Muito obrigado, não se esqueça de se inscrever, deixar a sua sugestão, um joinha, ativa o sininho e até o próximo vídeo.
