O Ubuntu 20.04 "Focal Fossa" foi lançado dia 23 de abril de 2020 pela Canonical. Por ser uma versão LTS, seu suporte irá até o ano de 2024.

Principais mudanças?
- GNOME 3.36
- Nova variação de tema
- Kernel 5.4
- Suporte aprimorado ao ZFS
- Ferramenta de desempenho GameMode pré-instalada
- Python 3 como padrão
- Suporte do BackGuard WireGuard

# Criando pendrive bootável no windows:
- Baixar o Rufus
- Selecionar o USB
- Selecionar a imagem
- Iniciar

# Criando pendrive bootável no ubuntu:
- Abrir a ferramenta pré-instalada para criação de dispositivos bootaveis
- Selecionar a imagem
- Selecionar o pendrive
- Iniciar

# Instalação
- Escolher o idioma e clicar em instalar o Ubuntu
- Selecionar o layout de teclado
- Selecionar se deseja um sistema minimal ou mais completo
- Realizar o particionamento, recomendável que escolha o automático
- Escolher a região
- Concluir a instalação