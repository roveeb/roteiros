# Informações

## Informações básicas sobre o vídeo
(Adicionar as informações sobre o vídeo em questão)
- Título:
- Categoria:
- Software:

## Links
(Adicionar os links citados no vídeo)
- Alterar
- Alterar
- Alterar

## Palavras chaves
(Adicionar as palavras chaves que é importante lembrar durante o vídeo)
- Alterar
- Alterar
- Alterar

## Fontes
(Adicionar as fontes usadas para pesquisas durante a construção do roteiro)
- Alterar
- Alterar
- Alterar

---

# Roteiro

## Introdução
(Primeira etapa do vídeo e a mais importante. Fazer um resumo chamativo para o conteúdo)

## Apresentação
(Realizar a apresentação do conteúdo e do apresentador. Essa etapa pode demorar mais e é onde deixamos o visualizador por dentro do conteúdo do vídeo. Ou seja, aqui entra um pouco de história para contextualizar)

## Chamada para ação
(Pedir inscrição, like e comentário)

## Desenvolvimento
(Realizar a instalação do software ou desenvolver o conteúdo. Essa etapa pode ser divida em várias etapas, como por exemplo: Instalação no Linux/Windows)

## Finalização
(Terminar o vídeo fazendo um resumo e finalizando a construção do conteúdo)
